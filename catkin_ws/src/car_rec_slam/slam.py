import rospy
import rosbag
import itertools

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Quaternion
from std_msgs.msg import Header

from inertiallabs_msgs.msg import ins_data

from tf.transformations import quaternion_from_euler
import tf.transformations
import std_msgs.msg
import geometry_msgs.msg
import sensor_msgs.msg
import time

bag = rosbag.Bag('/home/eeproj9/Desktop/2022-06-13-18-33-58.bag','r')

pub = rospy.Publisher('/transformed/imu',Imu, queue_size=10)
rospy.init_node('talker', anonymous=True)
msg_iter = itertools.islice(bag.read_messages(topics=['/sensor_sync/Inertial_Labs/ins_data']),1)
for count, (topic, m, t) in enumerate(msg_iter):
    print("{}: {}".format(count,m))
    print("{}: {}".format(count,m.YPR))
    msg = m
    x,y,z = msg.YPR.x,msg.YPR.y,msg.YPR.z
    q = quaternion_from_euler(x, y, z,'rzyx')
    quaternion = Quaternion(*q)
    imu = Imu(header=msg.header,orientation=quaternion_from_euler)
    imu.angular_velocity_covariance[0]=-1
    imu.linear_acceleration_covariance[0]=-1
    imu.header.frame_id="map"

    imu2 = sensor_msgs.msg.Imu(header=std_msgs.msg.Header(seq=m.header.seq,stamp=m.header.stamp,frame_id="map"),
    orientation=geometry_msgs.msg.Quaternion(msg.OriQuat.x,msg.OriQuat.y, msg.OriQuat.z, msg.OriQuat.w),angular_velocity_covariance=[-1.0,0,0,0,0,0,0,0,0],linear_acceleration_covariance=[-1.0,0,0,0,0,0,0,0,0])

    print("The quaternion representation is %s %s %s %s." % (q[0], q[1], q[2], q[3]))
    # print("The quaternion is {}".format(quaternion))
    # print("The imu is {}\n".format(imu))
    
    print("num of conncections {}".format(pub.get_num_connections()))
    pub.publish(imu2)
    print("published")

bag.close()
