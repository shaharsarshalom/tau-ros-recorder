import rospy
import itertools
import numpy as np

import tf
from sensor_msgs.msg import Imu
from std_msgs.msg import String
from inertiallabs_msgs.msg import ins_data


def callback(data):
    print(data)
    br = tf.TransformBroadcaster()
    o = data.orientation
    br.sendTransform((0,0,0),(o.x,o.y,o.z,o.w), data.header.stamp, "imutf", "world")

def callback2(data):
    br = tf.TransformBroadcaster()
    msg = data
    x,y,z = msg.YPR.x,msg.YPR.y,msg.YPR.z
    rads = np.deg2rad((x,y,z))
    print(rads)
    q = tf.transformations.quaternion_from_euler(rads[0], rads[1], rads[2],"rzyx")
    br.sendTransform((0,0,0),q, data.header.stamp, "enutf", "world")

    
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber("/transformed/imu", Imu, callback)
    # rospy.Subscriber("/sensor_playback/sensor_sync/Inertial_Labs/ins_data", ins_data, callback2)


    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

if __name__ == '__main__':
    listener()

