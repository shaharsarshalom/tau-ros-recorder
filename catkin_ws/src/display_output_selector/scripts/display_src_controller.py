#!/usr/bin/env python
from pickle import EMPTY_LIST
import rospy

from topic_tools.srv import MuxSelect
from display_output_selector.srv import set_display_output


mux2topic_dic = {
  "mux_lidar": "/velodyne_points",
  "mux_radar": "/as_tx/radar_markers",
  "mux_camera": "/image_raw",
  "mux_camera_comp": "/image_raw/compressed",
  "mux_ins_gps": "/Inertial_Labs/gps_data",
  "mux_ins_ins": "/Inertial_Labs/ins_data",
  "mux_ins_sensor": "/Inertial_Labs/sensor_data"
}

PLAYBACK_PREFIX = "/sensor_playback"
SYNC_PREFIX = "/sensor_sync"

class Display_Output_Selector(object):
    def __init__(self):
        pass



    def make_srv_proxies(self):
        failed_mux_lst = []
        for mux_node in mux2topic_dic.keys():
            print(mux_node)
            try:
                rospy.wait_for_service(mux_node + '/select',5)

            except rospy.ROSException as ros_error:
                failed_mux_lst.append(mux_node)
                rospy.loginfo(mux_node + " node error: " + str(ros_error))

        if failed_mux_lst == []:
            rospy.loginfo("Display Output Selector is runing, all muxes are on.")
        else:
            rospy.loginfo("Display Output Selector is runing, failed muxes: " + str(failed_mux_lst))

        self.mux_select_dic = {}

        for mux_node in mux2topic_dic.keys():
            if mux_node not in failed_mux_lst:
                self.mux_select_dic["%s" %mux_node] = rospy.ServiceProxy(mux_node + '/select', MuxSelect)

    def switch_source(self,req):

        """
        responce format:
        bool mux_camera_success # status of each mux
        bool mux_camera_comp_success
        bool mux_lidar_success
        bool mux_radar_success
        bool mux_ins_gps_success
        bool mux_ins_ins_success
        bool mux_ins_sensor_success
        str info
        """

        rospy.loginfo("display source switch node - Servies Called!")
        print("request = " + str(req.display))

        res_dic = {}
        turnoff_output_flag = False

        for mux in mux2topic_dic.keys():
            res_dic[mux] = False
   
        if req.display == "recording":
            print("Playing a recording")
            prefix = PLAYBACK_PREFIX

                        
        elif req.display == "livefeed":
            print("Playing Live Sensor data")
            prefix = ""

        elif req.display == "sync_recording":
            print("Playing a sync recording")
            prefix = PLAYBACK_PREFIX + SYNC_PREFIX
                
        elif req.display == "none":
            print("Displaying Nothing!")
            turnoff_output_flag = True

        else:
            print("BAD INPUT!")
            return (False,False,False,False,False,False,False,"Bad servies input!")

        for mux in self.mux_select_dic.keys():
            print("calling mux srv: " + mux[4:])
        
            if turnoff_output_flag:
                topic = "__none"
            else:
                topic = prefix + mux2topic_dic[mux]

            try:
                res = self.mux_select_dic[mux](topic)
                res_dic[mux] = True

            except (rospy.ServiceException, rospy.ROSException), ros_error:# as ros_error:
                print(mux + " error:" + str(ros_error))
                print("res: " + str(res))
                res_dic[mux] = False

        return (res_dic["mux_camera"],res_dic["mux_camera_comp"],res_dic["mux_lidar"],res_dic["mux_radar"],res_dic["mux_ins_gps"],res_dic["mux_ins_ins"],res_dic["mux_ins_sensor"],"Successful call")


        


def main():
    
    rospy.init_node("display_output_selector")
    mux_controller = Display_Output_Selector()
    mux_controller.make_srv_proxies()

    rospy.Service("set_display",set_display_output,mux_controller.switch_source)
    rospy.spin()

if __name__ == '__main__':
    main()