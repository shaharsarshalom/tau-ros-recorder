import os
import rospkg
import rospy

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget

from display_output_selector.srv import set_display_output

class FeedSwitch(Plugin):

    def __init__(self, context):
        super(FeedSwitch, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('Feed Switch')
        rp = rospkg.RosPack()

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print('arguments: ', args)
            print('unknowns: ', unknowns)

        # Create QWidget
        self._widget = QWidget()
        # Get path to UI file which is a sibling of this file
        # in this example the .ui and .py file are in the same folder
        ui_file = os.path.join(rp.get_path('rqt_feed_switch'), 'resource', 'feed_switch.ui')
        # Extend the widget with all attributes and children from UI file
        loadUi(ui_file, self._widget) # maybe remove the '.ui'
        # Give QObjects reasonable names
        self._widget.setObjectName('Feed Switch')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
        # Add widget to the user interface

        context.add_widget(self._widget)

        self._widget.rec_Button.setEnabled(False)
        self._widget.live_Button.setEnabled(False)
        self._widget.sync_CheckBox.setEnabled(False)
        
        srv_found = False
        for i in range(1):
            if self.is_servies_available():
                srv_found = True
                break
                
        
        self.display_switch_srv = rospy.ServiceProxy("/set_display", set_display_output)

        self._widget.rec_Button.clicked.connect(self.switch_to_recoding)
        self._widget.live_Button.clicked.connect(self.switch_to_livefeed)
        self._widget.sync_CheckBox.clicked.connect(self.switch_to_recoding)
        if srv_found:
            self._widget.rec_Button.setEnabled(True)
            self._widget.live_Button.setEnabled(True)


    def is_servies_available(self):
        try:
            rospy.loginfo("looking for '/set_display' servies")
            rospy.wait_for_service("/set_display",5)
                
        except rospy.ROSException as ros_error:
            rospy.loginfo("Count find display_output_selector servies '/set_display', error:" + str(ros_error))
            return False
        return True

    def switch_to_recoding(self):
        self._widget.sync_CheckBox.setEnabled(True)
        if self._widget.sync_CheckBox.isChecked():
            self.display_switch_srv("sync_recording")
        else:
            self.display_switch_srv("recording")

    def switch_to_livefeed(self):
        self._widget.sync_CheckBox.setEnabled(False)
        self.display_switch_srv("livefeed")
        
        



    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
