Velodyne LP16:
reference: http://wiki.ros.org/velodyne/Tutorials/Getting%20Started%20with%20the%20Velodyne%20VLP16

Check if you can reach the cofiguration page in the web browser at:
	http://192.168.1.201/
	
Installing ROS dependencies:
	$ sudo apt-get install ros-melodic-velodyne
	
Installing the VLP16 driver:
	$ cd ~/catkin_ws/src/ && git clone https://github.com/ros-drivers/velodyne.git
	$ rosdep install --from-paths src --ignore-src --rosdistro melodic -y
	
Build your workspace:
	$ cd ~/catkin_ws/ && catkin_make
	$ source devel/setup.bash
	
#Installation is complete.

How to View the Lidar Data: 
	
	$ roslaunch velodyne_pointcloud VLP16_points.launch
	
	$ rosrun rviz rviz -f velodyne 
	
	to view the points, add the '/velodyne_points' topic to the screen.
	
# NOTE:  we are interstered in the '/velodyne_points' topic to record our data!



	
	
	